// Requires Java 8 or newer

import java.io.*;
import java.nio.file.*;
import java.util.*;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

public class check
{
    // Number of SHA1 hashes in the filter. Must match the line count of the file.
    // wc -l test-sha1s.txt should equal this number
    private static int cardinality = 526235978;

    private static final String usage = "java -Xmx3072m -jar check.jar /path/to/filter.txt";

    private static TreeSet<String> notInFilter = new TreeSet<String>();

    /**
    * Load some strings (not in the filter) into a TreeSet
    **/
    public static void loadNotIn(String file_name)
    {
        try
        {
            FileInputStream fstream = new FileInputStream(file_name);
            DataInputStream input   = new DataInputStream(fstream);
            BufferedReader  buffer  = new BufferedReader(new InputStreamReader(input));

            String line;

            while ((line = buffer.readLine()) != null)
            {
                notInFilter.add( line );
            }

            input.close();
        }

        catch (Exception e) 
        {
            System.err.println("loadNotIn error: " + e.getMessage());
        }
    }

    /**
    * Read the filter into memory and return it.
    **/
    public static BloomFilter<byte[]> load_filter(String theFilter) throws FileNotFoundException, IOException
    {
        FileInputStream fis = new FileInputStream(new File(theFilter));
        return BloomFilter.readFrom(fis, Funnels.byteArrayFunnel());
    }

    /**
    * main
    **/
    public static void main(String[] args)
    {
        if (args.length == 1)
        {
            try {
                BloomFilter<byte[]> bf = BloomFilter.create(Funnels.byteArrayFunnel(), cardinality, 0.001);
                bf = load_filter(args[0]);

                // Some basic sanity checks to ensure the filter is returning valid results.
                // These should all pass before the filter is tagged and sent to Middleware.

                // Valid - in filter
                if( bf.mightContain("000000005AD76BD555C1D6D771DE417A4B87E4B4".getBytes("UTF-8")) != true ) {
                    System.out.println("000000005AD76BD555C1D6D771DE417A4B87E4B4 failed!");
                }

                // Invalid - not in filter
                if( bf.mightContain("w8rbt".getBytes("UTF-8")) != false ) {
                    System.out.println("w8rbt failed!");
                }

                // Invalid - not in filter (this looks like a FP)
                if( bf.mightContain("".getBytes("UTF-8")) != false ) {
                    System.out.println("empty string failed!");
                }

                // Invalid - not in filter
                if( bf.mightContain(" ".getBytes("UTF-8")) != false ) {
                    System.out.println("space failed!");
                }

                // Invalid - not in filter
                if( bf.mightContain("1".getBytes("UTF-8")) != false ) {
                    System.out.println("1 failed!");
                }

                // Invalid - not in filter
                if( bf.mightContain("-".getBytes("UTF-8")) != false ) {
                    System.out.println("- failed!");
                }

                // Valid - in filter
                if( bf.mightContain("FFFFFFFEE791CBAC0F6305CAF0CEE06BBE131160".getBytes("UTF-8")) != true ) {
                    System.out.println("FFFFFFFEE791CBAC0F6305CAF0CEE06BBE131160 failed!");
                }

                // Valid - in filter
                if( bf.mightContain("00000900444362A27C9422E2739928ACAB81A46C".getBytes("UTF-8")) != true ) {
                    System.out.println("00000900444362A27C9422E2739928ACAB81A46C failed!");
                }

                // Valid - in filter
                if( bf.mightContain("000450666C03CD1335ED376C54B0A39C74E5950D".getBytes("UTF-8")) != true ) {
                    System.out.println("000450666C03CD1335ED376C54B0A39C74E5950D failed!");
                }

                // Valid - in filter
                if( bf.mightContain("002B85D236E845760652DB6667C414DFF4D8E6C4".getBytes("UTF-8")) != true ) {
                    System.out.println("002B85D236E845760652DB6667C414DFF4D8E6C4 failed!");
                }

                // Valid - in filter
                if( bf.mightContain("016F2AC5A0D8051EC1116FF36149F01BE4D45FFE".getBytes("UTF-8")) != true ) {
                    System.out.println("016F2AC5A0D8051EC1116FF36149F01BE4D45FFE failed!");
                }

                // Invalid
                if( bf.mightContain("brad".getBytes("UTF-8")) != false ) {
                    System.out.println("brad failed!");
                }

                // Valid - in filter
                if( bf.mightContain("B02FB8231CEB585B6B9E911DDF3B9817971AE931".getBytes("UTF-8")) != true ) {
                    System.out.println("B02FB8231CEB585B6B9E911DDF3B9817971AE931 failed!");
                }

                // Invalid
                if( bf.mightContain("no".getBytes("UTF-8")) != false ) {
                    System.out.println("no failed!");
                }

                // Invalid
                if( bf.mightContain("nope".getBytes("UTF-8")) != false ) {
                    System.out.println("nope failed!");
                }

                // Invalid
                if( bf.mightContain("not".getBytes("UTF-8")) != false ) {
                    System.out.println("not failed!");
                }

                // Invalid - not in filter
                if( bf.mightContain("0000000000000000000000000000000000000000".getBytes("UTF-8")) != false ) {
                    System.out.println("0000000000000000000000000000000000000000 failed!");
                }

                // Invalid - not in filter
                if( bf.mightContain("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA".getBytes("UTF-8")) != false ) {
                    System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA failed!");
                }

                // Invalid
                if( bf.mightContain("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK".getBytes("UTF-8")) != false ) {
                    System.out.println("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK failed!");
                }
                
                // Invalid
                if( bf.mightContain("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT".getBytes("UTF-8")) != false ) {
                    System.out.println("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT failed!");
                }

                // Invalid - ZZ inserted into valid hash
                if( bf.mightContain("B02FB8231CEB585B6ZZE911DDF3B9817971AE931".getBytes("UTF-8")) != false ) {
                    System.out.println("B02FB8231CEB585B6ZZE911DDF3B9817971AE931 failed!");
                }

                // Invalid - Too big by 1
                if( bf.mightContain("B02FB8231CEB585B6B9E911DDF3B9817971AE9311".getBytes("UTF-8")) != false ) {
                    System.out.println("B02FB8231CEB585B6B9E911DDF3B9817971AE9311 failed!");
                }
                
                // Invalid - Too big by 2
                if( bf.mightContain("B02FB8231CEB585B6B9E911DDF3B9817971AE93112".getBytes("UTF-8")) != false ) {
                    System.out.println("B02FB8231CEB585B6B9E911DDF3B9817971AE93112 failed!");
                }

                // Invalid - Too big by 3
                if( bf.mightContain("B02FB8231CEB585B6B9E911DDF3B9817971AE931123".getBytes("UTF-8")) != false ) {
                    System.out.println("B02FB8231CEB585B6B9E911DDF3B9817971AE931123 failed!");
                }

                // Measure FP's
                loadNotIn("notIn.txt");
                for( String item : notInFilter )
                {
                    if( bf.mightContain(item.getBytes("UTF-8")) != false ) {
                        System.out.println(item + " is a FP");
                    }
                }

            } catch (FileNotFoundException e) {
                System.out.println(e); 
            } catch (IOException  e)  {
                System.out.println(e);
            }
        }

        else
        {
            System.out.println(usage);
        }
    }
}

