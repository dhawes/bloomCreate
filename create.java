// Requires Java 8 or newer

import java.io.*;
import java.nio.file.*;
import java.util.*;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

public class create
{
    // Number of SHA1 hashes to place in filter. Must match the line count of the file.
    // wc -l test-sha1s.txt should equal this number
    private static int cardinality = 526235978;

    private static final String usage = "java -jar create.jar /path/to/sha1/hashes.txt /path/to/output/filter.txt";
    private static BloomFilter<byte[]> bf = BloomFilter.create(Funnels.byteArrayFunnel(), cardinality, 0.001);

    /**
    * Load the hashes into the Bloom filter and save the filter as a file.
    **/
    public static void load_hashes(String fileOfSHA1Hashes, BloomFilter<byte[]> filter, String outputFilter)
    {
        try
        {
            FileInputStream fstream = new FileInputStream(fileOfSHA1Hashes);
            DataInputStream input   = new DataInputStream(fstream);
            BufferedReader  buffer  = new BufferedReader(new InputStreamReader(input));

            String line;

            while ((line = buffer.readLine()) != null)
            {
                filter.put( line.trim().getBytes() );
            }

            input.close();
        }

        catch (Exception e) 
        {
            System.err.println("load_hashes error: " + e.getMessage());
        }
   
        // Write the output filter to a file
        try
        {
            FileOutputStream fos = new FileOutputStream(outputFilter);
            filter.writeTo(fos);
            fos.close();
        }

        catch (Exception e)
        {
            System.err.println("Saving output filter error: " + e.getMessage());
        }
    }

    /**
    * main
    **/
    public static void main(String[] args)
    {
        if (args.length == 2)
        {
            load_hashes(args[0], bf, args[1]);
        }

        else
        {
            System.out.println(usage);
        }
    }
}

