# bloomCreate

Code, files and notes to create a small test bloom filter and a large production bloom filter for NIST 800-63-3B banned password checking.

## Test MD5 Checksums

  * 43c031e03b19fddc2f7a47ed7a27910c  test-filter.bin
  * 94208ba91a19f92cf75e639fd9540474  test-sha1s.txt
  * 9aae72e97fab4cad3033ab3e4323c84f  test-words.txt

## Test Notes

  * SHA1 hashes must be uppercase __ABCDEF__ rather than abcdef.
  * To re-create the hashes: __while read line; do echo -n $line | sha1sum >> test-sha1s.txt; done < test-words.txt__
  * Use vim to remove trailing '  -' from sha1sum output and uppercase the hashes (2000000V~).
  * To re-create the filter: __java -jar create.jar test-sha1s.txt test-filter.bin__
  * The __not files__ have words and hashes not found in the filter.

## Production MD5 Checksums

  * b2203a01c962a7863401346a6b9e3fc8  vt-banned-passwords-v1.filter

## Production Input Files (Stored in ITSO's AWS S3 instance due to size)

  1. __wordlist.txt__ - The original reserved password list (before the bloom filter).
  2. __wordlist-sha1-hashes.txt__ - The SHA1 hashes of the words in wordlist.txt.
  3. __pwned-passwords-ordered-by-count-just-hashes.txt__ - The HIBP v3 SHA1 hashes.
  4. __no.txt__ - Hashes from wordlist-sha1-hashes.txt that are not in the HIBP hashes.
  5. __combined-uniq-sha1-hashes.txt__ - no.txt combined with pwned-passwords-ordered-by-count-just-hashes.txt. 

## Production Notes

  * There are __526,235,978 unique SHA1 hashes__ in combined-uniq-sha1-hashes.txt
  * java -jar create.jar combined-uniq-sha1-hashes.txt vt-banned-passwords-v1.filter

