#!/bin/sh

echo "Compiling..."
javac -cp lib/guava-25.1-jre.jar create.java

echo "Creating jar..."
jar cfm create.jar Manifest.txt *.class

# Comment this out if you want to run
# the class files directly with java
echo "Removing class files..."
rm -f *.class

echo "Build Complete."

